//
//  HttpHandler.swift
//  schichtleiter
//
//  Created by Thomas Dubiel on 21.06.20.
//  Copyright © 2020 - All rights reserved.
//

import Foundation

protocol HttpHandlerDelegate {
   func authorizationErrorOccurred(error: RequestError)
}

public class HttpHandler: NSObject {
   
   var httpDelegate: HttpHandlerDelegate?
   var defaultSession: URLSession!
   
   override init() {
      super.init()
      defaultSession = URLSession(configuration: .default, delegate: self, delegateQueue: OperationQueue())
   }
   
   /**
   Starts a new URL Request with the given URL-string and receives an Array of Data from the Backend
   - Parameter urlString: URL string to access the backend
   - Parameter completion: The closure which returns the received data as an Array of [String: Any] or an error
   */
   func getDataFromBackend(_ urlString: String, completion: @escaping (Result<Data, RequestError>) -> Void) {
      guard let url = URL(string: urlString) else {
         completion(.failure(.emptyURL))
         return
      }

      var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10.0 * 1000)
      request.httpMethod = "GET"

      defaultSession = URLSession(configuration: .default, delegate: self, delegateQueue: OperationQueue())
      let task = defaultSession.dataTask(with: request) { (data, response, error) -> Void in

         guard error == nil else {
            let nsError = error as NSError?
            print("Error: \(String(describing: nsError))")
            completion(.failure(.unhandledError(status: error!)))
            return
         }

         if (response as? HTTPURLResponse)?.statusCode == 200 {
            guard let data = data else {
               print("Nil data received from web service")
               completion(.failure(.emptyResultData))
               return
            }
            completion(.success(data))
         }
      }

      task.resume()
   }
   
   /**
   Starts a new URL Request with the given URL-string and put an Array of Data to the Backend
   - Parameter urlString: URL string to access the backend
   - Parameter jsonData: JSON data which should be send by the put request
   - Parameter completion: The closure which returns a boolean value or an error
   */
   func putDataToBackend(urlString: String, jsonData: Data, completion: @escaping (Result<Bool, RequestError>) -> Void) {
      guard let url = URL(string: urlString) else {
         completion(.failure(.emptyURL))
         return
      }

      var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10.0 * 1000)
      request.setValue("application/json", forHTTPHeaderField: "Content-Type")
      //request.setValue("\(NSData(data: jsonData).length)", forHTTPHeaderField: "Content-Length")
      request.httpMethod = "PUT"
      request.httpBody = jsonData

      let session = URLSession(configuration: .default, delegate: self, delegateQueue: OperationQueue())

      let task = session.dataTask(with: request) { (data, response, error) -> Void in

         guard error == nil else {
            let nsError = error as NSError?
            print("Error: \(String(describing: nsError))")
            completion(.failure(.unhandledError(status: error!)))
            return
         }

         if (response as? HTTPURLResponse)?.statusCode == 200 {
            completion(.success(true))
         } else {
            let statusCode = (response as? HTTPURLResponse)?.statusCode ?? 999
            print("Http Status Code: \(statusCode), Response: \(String(describing: response))")
            completion(.failure(.badHttpStatusCode(code: statusCode)))
         }
      }

      task.resume()
   }
   
   /**
   Starts a new URL Request with the given URL-string and put an Array of Data to the Backend
   - Parameter urlString: URL string to access the backend
   - Parameter jsonData: JSON data which should be send by the put request
   - Parameter completion: The closure which returns a boolean value or an error
   */
   func postDataToBackend(urlString: String, jsonData: Data, completion: @escaping (Result<Bool, RequestError>) -> Void) {
      guard let url = URL(string: urlString) else {
         completion(.failure(.emptyURL))
         return
      }
      
      var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10.0 * 1000)
      request.setValue("application/json", forHTTPHeaderField: "Content-Type")
      //request.setValue("\(NSData(data: jsonData).length)", forHTTPHeaderField: "Content-Length")
      request.httpMethod = "POST"
      request.httpBody = jsonData
      
      let session = URLSession(configuration: .default, delegate: self, delegateQueue: OperationQueue())
      
      let task = session.dataTask(with: request) { (data, response, error) in
         guard error == nil else {
            
            let nsError = error as NSError?
            print("Error: \(String(describing: nsError))")
            completion(.failure(.unhandledError(status: error!)))
            return
         }
         if (response as? HTTPURLResponse)?.statusCode == 200 {
            completion(.success(true))
         } else {
            let statusCode = (response as? HTTPURLResponse)?.statusCode ?? 999
            print("Http Status Code: \(statusCode), Response: \(String(describing: response))")
            completion(.failure(.badHttpStatusCode(code: statusCode)))
         }
      }
      
      task.resume()
   }
   
}

extension HttpHandler: URLSessionDelegate {

   public func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> ()) {
      
      // in production it looks much better ;-)
      let comUser: String? = "evil"
      let comPassword: String? = "verry secret"
      
      if challenge.protectionSpace.authenticationMethod == "NSURLAuthenticationMethodNTLM" {

         guard let comUser = comUser, let comPassword = comPassword else {
            DispatchQueue.main.async {
               self.httpDelegate?.authorizationErrorOccurred(error: .noCredentialsFound)
            }
            return
         }

         let credentials = URLCredential(user: comUser, password: comPassword, persistence: .forSession)
         completionHandler(.useCredential, credentials)
      } else if challenge.protectionSpace.authenticationMethod == "NSURLAuthenticationMethodServerTrust" {
         completionHandler(.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
      }
   }
}
