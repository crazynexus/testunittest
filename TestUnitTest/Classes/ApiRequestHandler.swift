//
//  ApiRequestHandler.swift
//  schichtleiter
//
//  Created by Thomas Dubiel on 11.02.20.
//  Copyright © 2020 - All rights reserved.
//

import Foundation

protocol ApiRequestHandlerDelegate {
   func authorizationErrorOccurred(error: RequestError)
}

class ApiRequestHandler: NSObject /*, URLSessionDelegate */ {

   // MARK: - Type Alias
   typealias JSONDictionary = [String: Any]
   typealias QueryResult = ([String: Any]?) -> Void

   private var errorMsg: RequestError?

   var apiDelegate: ApiRequestHandlerDelegate?
   var httpHandler = HttpHandler()
   
   override init() {
      super.init()
      
      httpHandler = HttpHandler()
      httpHandler.httpDelegate = self
   }

   func getSockInformation(for orderId: String, completion: @escaping (Result<StockInfo, RequestError>) -> Void) {
      errorMsg = nil
      let urlString = SettingsHandler().getUrl + AppConstants.STOCK_INFO_API + orderId

      httpHandler.getDataFromBackend(urlString) { result in
         switch result {
         case .success(let data):
            guard let json = try? JSONSerialization.jsonObject(with: data) as? [String : Any]  else {
               completion(.failure(.badJsonDataFound))
               return
            }
            
            let stockInfo = StockInfo(orderInfo: self.getOrderInfo(json),
                    orderString: json[AppConstants.ORDER_STRING] as? String ?? "",
                    lastTM: json[AppConstants.LAST_TM] as? String ?? "",
                    lastStock: json[AppConstants.LAST_STOCK] as? String ?? "",
                    resultText: json[AppConstants.RESULT_TEXT] as? String ?? ""
            )
            
            DispatchQueue.main.async {
               completion(.success(stockInfo))
            }
            
         case .failure(let error):
            DispatchQueue.main.async {
               completion(.failure(error))
            }
         }
      }
   }

//   func getHookInformation(for orderId: String, completion: @escaping (Result<Hooks, RequestError>) -> Void) {
//      errorMsg = nil
//      let urlString = SettingsHandler().getUrl + AppConstants.HOOK_INFO_API + orderId
//
//      HttpHandler().getDataFromBackend(urlString) { result in
//         switch result {
//         case .success(let data):
//            guard let json = try? JSONSerialization.jsonObject(with: data) as? [String : Any]  else {
//               completion(.failure(.badJsonDataFound))
//               return
//            }
//
//            let hookInfo = Hooks(orderInfo: self.getOrderInfo(json),
//                    orderId: json[AppConstants.ORDER_ID] as? Int ?? 0,
//                    orderString: json[AppConstants.ORDER_STRING] as? String ?? "",
//                    orderAcceptanceFinished: json[AppConstants.ORDER_ACCEPTANCE_FINISHED] as! Bool,
//                    orderDelivery: json[AppConstants.ORDER_DELIVERED] as! Bool,
//                    itemProduced: json[AppConstants.ITEM_PRODUCED] as! Bool,
//                    itemExpedited: json[AppConstants.ITEM_EXPEDITED] as! Bool,
//                    itemDelivered: json[AppConstants.ITEM_DELIVERED] as! Bool
//            )
//
//            DispatchQueue.main.async {
//               completion(.success(hookInfo))
//            }
//
//         case .failure(let error):
//            DispatchQueue.main.async {
//               completion(.failure(error))
//            }
//         }
//      }
//   }
//
//   func getBagInformation(for orderId: String, completion: @escaping (Result<BagInfo, RequestError>) -> Void) {
//      errorMsg = nil
//      let urlString = SettingsHandler().getUrl + AppConstants.BAG_INFO_API + orderId
//
//      HttpHandler().getDataFromBackend(urlString) { result in
//         switch result {
//         case .success(let data):
//            guard let json = try? JSONSerialization.jsonObject(with: data) as? [String : Any]  else {
//               completion(.failure(.badJsonDataFound))
//               return
//            }
//
//            let bagInfo = BagInfo(orderInfo: self.getOrderInfo(json),
//                    orderId: json[AppConstants.ORDER_STRING] as? String ?? "",
//                    mot: json[AppConstants.MOT] as? Array<String>,
//                    motStockNumber: json[AppConstants.MOT_STOCK_NUMBER] as? String,
//                    bags: json[AppConstants.BAGS] as? Array<String>,
//                    bagStockNumber: json[AppConstants.BAGS_STOCK_NUMBER] as? String,
//                    container: json[AppConstants.CONTAINER] as? Array<String>
//            )
//
//            DispatchQueue.main.async {
//               completion(.success(bagInfo))
//            }
//
//         case .failure(let error):
//            DispatchQueue.main.async {
//               completion(.failure(error))
//            }
//         }
//      }
//   }
//
//   func getAvailablePrinter(completion: @escaping (Result<[Printer], RequestError>) -> Void) {
//
//      let urlString = SettingsHandler().getUrl + AppConstants.PRINT_INFO_API
//
//      httpHandler.getDataFromBackend(urlString) { (result: Result<Data, RequestError>) in
//         switch result {
//         case .success(let data):
//            guard let json = try? JSONSerialization.jsonObject(with: data) as? [JSONDictionary] else {
//               completion(.failure(.badJsonDataFound))
//               return
//            }
//
//            var printerList = [Printer]()
//            for aPrinter in json {
//               printerList.append(Printer(name: aPrinter[AppConstants.PRINTER_NAME] as? String ?? "",
//                       network: aPrinter[AppConstants.PRINTER_NETWORK] as! Bool))
//            }
//
//            DispatchQueue.main.async {
//               completion(.success(printerList))
//            }
//
//         case .failure(let error):
//            DispatchQueue.main.async {
//               completion(.failure(error))
//            }
//         }
//      }
//   }
//
//   func sendHookInformation(hookInfo: Hooks, completion: @escaping (Result<Bool, RequestError>) -> Void) {
//      errorMsg = nil
//      let urlString = SettingsHandler().getUrl + AppConstants.HOOK_PUT_API
//
//      let json: [String: Any] = [AppConstants.ORDER_ID as String: hookInfo.orderId as Int,
//                                 AppConstants.ORDER_STRING as String: hookInfo.orderString as String,
//                                 AppConstants.ORDER_ACCEPTANCE_FINISHED as String: hookInfo.orderAcceptanceFinished as Bool,
//                                 AppConstants.ORDER_DELIVERED as String: hookInfo.orderDelivery as Bool,
//                                 AppConstants.ITEM_PRODUCED as String: hookInfo.itemProduced as Bool,
//                                 AppConstants.ITEM_EXPEDITED as String: hookInfo.itemExpedited as Bool,
//                                 AppConstants.ITEM_DELIVERED as String: hookInfo.itemDelivered as Bool
//      ]
//
//      guard let jsonData = try? JSONSerialization.data(withJSONObject: json) else {
//         completion(.failure(.noJsonDataToSend))
//         return
//      }
//
//      httpHandler.putDataToBackend(urlString: urlString, jsonData: jsonData) { result in
//         DispatchQueue.main.async {
//            completion(result)
//         }
//      }
//   }
//
//   func sendBagsAndContainerInformation(bagInfo: BagInfo, completion: @escaping (Result<Bool, RequestError>) -> Void) {
//      let urlString = SettingsHandler().getUrl + AppConstants.BAG_PUT_API
//
//      let json: [String: Any] = [AppConstants.ORDER_STRING as String: bagInfo.orderId as String,
//                                 AppConstants.MOT as String: bagInfo.mot as NSArray? as Any,
//                                 AppConstants.MOT_STOCK_NUMBER as String: bagInfo.motStockNumber as Any,
//                                 AppConstants.BAGS as String: bagInfo.bags as NSArray? as Any,
//                                 AppConstants.BAGS_STOCK_NUMBER as String: bagInfo.bagStockNumber as Any,
//                                 AppConstants.CONTAINER as String: bagInfo.container as NSArray? as Any
//      ]
//
//      guard let jsonData = try? JSONSerialization.data(withJSONObject: json) else {
//         completion(.failure(.noJsonDataToSend))
//         return
//      }
//
//      httpHandler.putDataToBackend(urlString: urlString, jsonData: jsonData) { result in
//         DispatchQueue.main.async {
//            completion(result)
//         }
//      }
//   }
//
//   func sendPrintRequest(orderID: String, printer: String, copies: Int, completion: @escaping (Result<Bool, RequestError>) -> Void) {
//      let urlSting = SettingsHandler().getUrl + AppConstants.PRINT_POST_API
//
//      let json: [String: Any] = [AppConstants.PRINT_ORDER_ITEM as String: orderID as String,
//                                 AppConstants.PRINT_PRINTER as String: printer as String,
//                                 AppConstants.PRINT_COUNT as String: copies as Int
//      ]
//
//      guard let jsonData = try? JSONSerialization.data(withJSONObject: json) else {
//         completion(.failure(.noJsonDataToSend))
//         return
//      }
//
//      httpHandler.postDataToBackend(urlString: urlSting, jsonData: jsonData) { (result) in
//         DispatchQueue.main.async {
//            completion(.success(true))
//         }
//      }
//   }

   // MARK: - Private Methods

   private func getOrderInfo(_ result: [String: Any]) -> OrderInfo {
      OrderInfo(customerNumber: result[AppConstants.CUSTOMER_NUMBER] as! Int,
              customerName: result[AppConstants.CUSTOMER_NAME] as? String ?? "",
              costCenter: result[AppConstants.COSTCENTER_NUMBER] as! Int,
              costCenterName: result[AppConstants.COSTCENTER_NAME] as? String ?? "")
   }

}

extension ApiRequestHandler: HttpHandlerDelegate {
   
   func authorizationErrorOccurred(error: RequestError) {
      apiDelegate?.authorizationErrorOccurred(error: error)
   }
   
}

extension ApiRequestHandler: URLSessionDelegate {

   public func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> ()) {
      
      // in production it looks much better ;-)
      let comUser: String? = "evil"
      let comPassword: String? = "verry secret"
      
      if challenge.protectionSpace.authenticationMethod == "NSURLAuthenticationMethodNTLM" {

         guard let comUser = comUser, let comPassword = comPassword else {
            DispatchQueue.main.async {
               self.apiDelegate?.authorizationErrorOccurred(error: .noCredentialsFound)
            }
            return
         }

         let credentials = URLCredential(user: comUser, password: comPassword, persistence: .forSession)
         completionHandler(.useCredential, credentials)
      } else if challenge.protectionSpace.authenticationMethod == "NSURLAuthenticationMethodServerTrust" {
         completionHandler(.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
      }
   }
}
