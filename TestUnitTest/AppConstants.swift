//
//  AppConstants.swift
//  TestUnitTest
//
//  Created by Thomas Dubiel on 17.07.20.
//  Copyright © 2020 Crazy Nexus. All rights reserved.
//

import Foundation

// MARK: Types

enum RequestError: Error {
   case emptyURL
   case emptyResultData
   case badJsonDataFound
   case noCredentialsFound
   case noJsonDataToSend
   case unhandledError(status: Error)
   case badHttpStatusCode(code: Int)
}

class AppConstants: NSObject {
   
   static let DEFAULT_URL = "https://127.0.0.1:36436/api/"
   static let URL_STRING = "defaultUrl"
   static let COM_USER = "communicationUser"
   
   static let STOCK_INFO_API = "Posten/InfoLagerTransportmittel?OrderString="
   
   // MARK: - Common Dictionary Keys
   static let ORDER_STRING = "OrderString"
   static let CUSTOMER_NUMBER = "Kundennummer"
   static let CUSTOMER_NAME = "Kundenbezeichnung"
   static let COSTCENTER_NUMBER = "Kostenstelle"
   static let COSTCENTER_NAME = "Kostenstellenbezeichnung"
   
   // MARK: - StockInfo Dictionary Keys
   static let LAST_TM = "LetztesTransportmittel"
   static let LAST_STOCK = "LetztesLager"
   static let RESULT_TEXT = "ResultText"
   
}
