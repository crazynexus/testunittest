//
//  SettingsHandler.swift
//  schichtleiter
//
//  Created by Thomas Dubiel on 05.02.20.
//  Copyright © 2020 - All rights reserved.
//

import Foundation

struct SettingsHandler {

   var userDefaults: UserDefaults = UserDefaults.standard
   private var oldUser: String?

   var getUrl: String {
      get {
         let urlStr = userDefaults.value(forKey: AppConstants.URL_STRING) as? String

         guard let url = urlStr else {
            return AppConstants.DEFAULT_URL
         }

         return url
      }
      set(newValue) {
         if newValue == "" {
            userDefaults.removeObject(forKey: AppConstants.URL_STRING)
         } else {
            var url = newValue
            
            // check and correct user input in case the last char is no "/"
            let lastChar = newValue.last
            if lastChar != "/" {
               url = url + "/"
            }
            
            userDefaults.setValue(url, forKey: AppConstants.URL_STRING)
         }
      }
   }
}
