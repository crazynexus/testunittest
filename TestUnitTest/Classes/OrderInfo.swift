//
//  OrderInfo.swift
//  schichtleiter
//
// Created by Thomas Dubiel on 07.05.20.
// Copyright (c) 2020 - All rights reserved.
//

import Foundation

struct OrderInfo {
   var customerNumber: Int
   var customerName: String
   var costCenter: Int
   var costCenterName: String
}
