//
//  TestUnitTestTests.swift
//  TestUnitTestTests
//
//  Created by Thomas Dubiel on 17.07.20.
//  Copyright © 2020 Crazy Nexus. All rights reserved.
//

import XCTest
@testable import TestUnitTest

class MockUserDefaults: UserDefaults {
   var defaultUrl: Any?
   var comUser: Any?
   
   override func setValue(_ value: Any?, forKey key: String) {
      switch key {
      case AppConstants.URL_STRING:
         defaultUrl = value
      case AppConstants.COM_USER:
         comUser = value
      default:
         break
      }
   }
   
   override func value(forKey key: String) -> Any? {
      switch key {
      case AppConstants.URL_STRING:
         return defaultUrl
      case AppConstants.COM_USER:
         return comUser
      default:
         return nil
      }
   }
}

class TestUnitTestTests: XCTestCase {

   var sut: SettingsHandler!
   var mockDefaults: MockUserDefaults!

   override func setUp() {
      super.setUp()
      mockDefaults = MockUserDefaults()
      
      sut = SettingsHandler()
      sut.userDefaults = mockDefaults
      
   }

   override func tearDown() {
      sut = nil
      mockDefaults = nil
      
      super.tearDown()
   }

   // MARK: - UserSettings Unit Tests
   
   func testDefaultUrl() {
      // given
      let testUrl = "https://test.devils-kittchen.com/api/testUrl"
      XCTAssertEqual(sut.getUrl, AppConstants.DEFAULT_URL, "Expected Default URL not as expected!")

      // when
      sut.getUrl = testUrl
      
      // then
      XCTAssertEqual(mockDefaults.defaultUrl as! String, "\(testUrl)/", "Default URL was not set correctly")
      XCTAssertEqual(sut.getUrl, "\(testUrl)/", "Default URL was not changed correctly")
   }
   
}
