//
//  StockInfo.swift
//  schichtleiter
//
//  Created by Thomas Dubiel on 13.02.20.
//  Copyright © 2020 - All rights reserved.
//

import Foundation

struct StockInfo {
   var orderInfo: OrderInfo

   var orderString: String
   var lastTM: String
   var lastStock: String
   var resultText: String
}
