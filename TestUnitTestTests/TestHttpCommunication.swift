//
//  TestHttpCommunication.swift
//  TestUnitTestTests
//
//  Created by Thomas Dubiel on 17.07.20.
//  Copyright © 2020 Crazy Nexus. All rights reserved.
//

import XCTest
@testable import TestUnitTest

class HttpHandlerMoc: HttpHandler {
   
   override func getDataFromBackend(_ urlString: String, completion: @escaping (Result<Data, RequestError>) -> Void) {
      let testBundle = Bundle(for: type(of: self))
      let path = testBundle.path(forResource: "Printer", ofType: "json")
      let data = try? Data(contentsOf: URL(fileURLWithPath: path!), options: .alwaysMapped)
      
      completion(.success(data!))
   }
}

class TestHttpCommunication: XCTestCase {
   
   var sut: ApiRequestHandler!

   override func setUpWithError() throws {
   
      try super.setUpWithError()
      
      sut = ApiRequestHandler()

   }

   override func tearDownWithError() throws {
      sut = nil
      try super.tearDownWithError()
   }

   func testAvailablePrinter() {
      // given
      let mocHandler = HttpHandlerMoc()
      
      sut.httpHandler = mocHandler as! HttpHandler
      
      // when
      sut.getSockInformation(for: "test") { results in
         switch results {
         case .success(let stockInfo):
            break
         case .failure(let error):
            break
         }
      }
      // then
   }

}
